import { browser } from '$app/environment';
import { writable } from 'svelte/store';

const defaultValue = JSON.stringify({
	mute: false,
	allTypes: true,
	pokemonTypes: [],
	pokemonSelected: {},
	pokemonFiltered: [],
	pokemonList: [],
	filterWeight: 0,
	filterHeight: 0,
	selectedTypes: []
});

let initialValue = defaultValue;

 if (browser) {
	initialValue = window.localStorage.getItem('pokemon');
}

const storage = writable(initialValue);

storage.subscribe((value) => {
   if (browser) {
      let newValue= value;
      if (typeof newValue != 'string') newValue = JSON.stringify(value);
		window.localStorage.setItem('pokemon', newValue);
	}
});

export { storage, defaultValue };
